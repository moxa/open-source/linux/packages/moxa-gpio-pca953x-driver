# moxa-gpio-pca953x-driver

## PCA953x 16-bit I/O expander
the PCA953x 16-bit I/O expander driver based on
https://github.com/torvalds/linux/blob/v5.10/drivers/gpio/gpio-pca953x.c

### Compile & install the driver

#### Install build-essential packages
```
sudo yum install build-essential linux-headers-`uname -r`
```

#### Compile the driver
```
make
```

#### Compile and install the driver
```
make install
depmod -a
```

#### Load the watchdog driver
```
modprobe gpio-pca953x
```
